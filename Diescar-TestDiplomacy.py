#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):

        s = StringIO("A Houston Move Austin\nB Dallas Hold\nC Waco Support A\n")
        result = diplomacy_read(s)
        self.assertEqual(result, [["A", "Houston", "Move", "Austin"],\
                                  ["B", "Dallas", "Hold"], ["C", "Waco", "Support", "A"]])
        
    def test_read_2(self):

        s = StringIO("A Texas Support B\nB Ohio Support A\nC Arizona Move Texas\n")
        result = diplomacy_read(s)
        test = [["A", "Texas", "Support", "B"], ["B", "Ohio", "Support", "A"], \
                                  ["C", "Arizona", "Move", "Texas"]]
        self.assertEqual(result, test)

    def test_read_3(self):
        s = StringIO("B ElPaso Hold\nC SanAntonio Hold\n")
        result = diplomacy_read(s)
        self.assertEqual(result, [["B", "ElPaso", "Hold"], ["C", "SanAntonio", "Hold"]])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        test = [["A", "Houston", "Move", "Austin"], \
                                  ["B", "Dallas", "Hold"], ["C", "Waco", "Support", "A"]]
        v = diplomacy_eval(test)
        self.assertEqual(v, ["A Austin\n", "B Dallas\n", "C Waco\n"])
       

    def test_eval_2(self):
        test = [["A", "Texas", "Support", "B"], ["B", "Ohio", "Support", "A"], \
                                  ["C", "Arizona", "Move", "Texas"]]
        v = diplomacy_eval(test)
        self.assertEqual(v, ["A Texas\n", "B Ohio\n", "C [dead]\n"])


    def test_eval_3(self):
        test = [["C", "ElPaso", "Hold"], ["B", "SanAntonio", "Hold"]]
        v = diplomacy_eval(test)
        self.assertEqual(v, ["B SanAntonio\n", "C ElPaso\n"])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A Austin\n", "B Dallas\n", "C Waco\n"])
        self.assertEqual(w.getvalue(), "A Austin\nB Dallas\nC Waco\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A Texas\n", "B Ohio\n", "C [dead]\n"])
        self.assertEqual(w.getvalue(), "A Texas\nB Ohio\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["B SanAntonio\n", "C ElPaso\n"])
        self.assertEqual(w.getvalue(), "B SanAntonio\nC ElPaso\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Houston Move Austin\nB Dallas Hold\nC Waco Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Dallas\nC Waco\n")

    def test_solve_2(self):
        r = StringIO("A Texas Support B\nB Ohio Support A\nC Arizona Move Texas\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Texas\nB Ohio\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("B ElPaso Hold\nC SanAntonio Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "B ElPaso\nC SanAntonio\n")

    def test_solve_4(self):
        r = StringIO("A Houston Hold\nB SanAntonio Move Houston\nC Dallas Move Houston\nD Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\nC [dead]\nD Austin\n")

    def test_solve_5(self):
        r = StringIO("A Houston Hold\nB SanAntonio Move Houston\nC Dallas Move Houston\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
